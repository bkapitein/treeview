export interface Organization
{
  id: number;
  name: string;
  hasChildren: boolean;
}