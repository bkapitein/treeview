export interface Account
{
  id: number;
  name: string;
  isAccount: boolean;
}