import { forkJoin, map, Observable, of, tap } from "rxjs";
import { Account } from "./models/account";
import { Organization } from "./models/organization";
import { OrganizationService } from "./organization.service";
import { TreeNode } from "./tree-list/tree-node";
import { TreeNodeDataSource } from "./tree-list/tree-node-data-source";

export class OrganizationDataSource extends TreeNodeDataSource<Organization | Account>
{
    constructor(private readonly organizationService: OrganizationService) {
        super();
    }

    protected createTreeNode(item: Organization | Account, level: number): TreeNode<Organization | Account>
    {
        const isAccount = (<Account>item).isAccount;
        const hasChildren = (<Organization>item).hasChildren;
        return new TreeNode<Organization | Account>(item.name, isAccount ? 'work' : 'corporate_fare', item, level, hasChildren);
    }

    protected getRootItems(): Observable<(Organization | Account)[]> {
        return this.organizationService.getOrganizations(undefined);
    }

    protected getChildren(parent?: Organization | Account) {
        if(!(<Organization>parent).hasChildren) {
            return of([]);
        }

        return forkJoin([
            this.organizationService.getOrganizations(<Organization>parent),
            this.organizationService.getAccounts(<Organization>parent),
        ]).pipe(
            map(([orgs, accounts]) => [...orgs, ...accounts])
        );
    }
}