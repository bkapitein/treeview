import { Injectable } from '@angular/core';
import { delay, Observable, of, tap } from 'rxjs';
import { Account } from './models/account';
import { Organization } from './models/organization';
import { faker } from '@faker-js/faker';

@Injectable({
  providedIn: 'root'
})
export class OrganizationService {

  constructor() { }

  public getOrganizations(parent?: Organization): Observable<Organization[]>
  {
    console.log(`Loading fake child organizations of ${parent?.name}`);
    return of(
      Array.from({length: 4}, (_, id) => <Organization> {
        id: id,
        name: faker.company.companyName(),
        hasChildren: true
      })
    ).pipe(
      delay(700),
    );
  }

  public getAccounts(parent: Organization): Observable<Account[]>
  {
    console.log(`Loading fake accounts of ${parent?.name}`);
    return of(
      Array.from({length: 4}, (_, id) => <Account> {
        id: id,
        isAccount: true,
        name: faker.company.companyName(),
      })
    ).pipe(
      delay(700),
    );
  }
}
