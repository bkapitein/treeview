import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OrganizationTreeListComponent } from './organization-tree-list.component';

describe('OrganizationTreeListComponent', () => {
  let component: OrganizationTreeListComponent;
  let fixture: ComponentFixture<OrganizationTreeListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OrganizationTreeListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OrganizationTreeListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
