import { Component } from '@angular/core';
import { OrganizationDataSource } from '../organization-data-source';
import { OrganizationService } from '../organization.service';

@Component({
  selector: 'app-organization-tree-list',
  templateUrl: './organization-tree-list.component.html',
  styleUrls: ['./organization-tree-list.component.scss']
})
export class OrganizationTreeListComponent {
  dataSource: OrganizationDataSource;
  
  constructor(organizationService: OrganizationService) {
    this.dataSource = new OrganizationDataSource(organizationService);
  }
}
