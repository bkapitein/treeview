export class TreeNode<T> {
    constructor(
      public name: string,
      public iconName: string,
      public item?: T,
      public level = 0,
      public hasChildren = false,
      public isLoading = false,
      public childsLoaded = false,
      public isExpanded = false
    ) {}
  }
  