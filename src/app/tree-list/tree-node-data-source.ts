import { CollectionViewer, DataSource } from "@angular/cdk/collections";
import { FlatTreeControl } from "@angular/cdk/tree";
import { BehaviorSubject, filter, forkJoin, map, merge, Observable, of, switchMap, tap } from "rxjs";
import { TreeNode } from "./tree-node";

export abstract class TreeNodeDataSource<T> extends DataSource<TreeNode<T>>
{
    public readonly treeControl: FlatTreeControl<TreeNode<T>>;
    private readonly treeNodes = new BehaviorSubject<TreeNode<T>[]>([]);
    private readonly nodesExpanded: Observable<TreeNode<T>[]>;
    private readonly nodesCollapsed: Observable<TreeNode<T>[]>;

    get data() {
        return this.filterCollapsedNodes(this.treeNodes.value);
    }

    constructor() {
        super();
        this.treeControl = new FlatTreeControl<TreeNode<T>>(dataNode => dataNode.level, dataNode => dataNode.hasChildren);
        
        this.nodesExpanded = this.treeControl.expansionModel.changed.pipe(
            filter(changeEvent => changeEvent.added.length > 0),
            map(changeEvent => changeEvent.added),
            switchMap(treeNodes => this.expandNodes(treeNodes)),
        );

        this.nodesCollapsed = this.treeControl.expansionModel.changed.pipe(
            filter(changeEvent => changeEvent.removed.length > 0),
            map(changeEvent => changeEvent.removed),
            map(treeNodes => this.collapseNodes(treeNodes)),
        );
    }

    connect(collectionViewer: CollectionViewer): Observable<TreeNode<T>[]> {
        return merge(...([
                collectionViewer.viewChange,
                this.loadRootNodes(),
                this.nodesExpanded,
                this.nodesCollapsed,
            ] as Observable<unknown>[]))
        .pipe(
            map(() => this.data),
        );
    }

    disconnect(): void {
    }

    protected abstract getRootItems(): Observable<T[]>;

    protected abstract getChildren(parent?: T): Observable<T[]>;

    protected abstract createTreeNode(item: T, level: number): TreeNode<T>;

    protected loadRootNodes(): Observable<TreeNode<T>[]> {
        return this.getRootItems().pipe(
            map(items => this.createTreeNodes(items, 1)),
            tap((rootNodes) => this.treeNodes.next(rootNodes)),
        );
    }

    private expandNodes(nodes: TreeNode<T>[]): Observable<TreeNode<T>[]>
    {
        return forkJoin(
            nodes.map(node => {
                node.isExpanded = true;
                return this.mergeChildNodes(node);
            })
        ).pipe(
            map(() => [...this.treeNodes.value]),
        );
    }

    private collapseNodes(nodes: TreeNode<T>[]): TreeNode<T>[]
    {
        nodes.forEach(node => node.isExpanded = false);
        return this.treeNodes.value;
    }

    private mergeChildNodes(parentNode: TreeNode<T>): Observable<TreeNode<T>[]> {
        if(!parentNode.hasChildren || parentNode.childsLoaded) {
            return of([...this.treeNodes.value]);
        }

        const parentIndex = this.treeNodes.value.indexOf(parentNode);
        const allTreeNodes = [...this.treeNodes.value];

        parentNode.isLoading = true;
        return this.getChildNodes(parentNode).pipe(
            tap(() => parentNode.childsLoaded = true),
            tap(() => parentNode.isLoading = false),
            map(childNodes => allTreeNodes.splice(parentIndex + 1, 0, ...childNodes) && allTreeNodes),
            tap(treeNodes => this.treeNodes.next(treeNodes)),
        );
    }

    protected getChildNodes(parentNode: TreeNode<T>): Observable<TreeNode<T>[]> {

        return this.getChildren(parentNode.item).pipe(
            map(items => this.createTreeNodes(items, parentNode.level + 1))
        );
    }
    
    protected createTreeNodes(items: T[], level: number): TreeNode<T>[] {
        return items.map((item) => this.createTreeNode(item, level));
    }
    
    public hasChildren(index: number, node: TreeNode<T>): boolean
    {
        return node.hasChildren;
    }

    protected filterCollapsedNodes(allTreeNodes: TreeNode<T>[]): TreeNode<T>[]
    {
        const expandedNodes: TreeNode<T>[] = [];
        let currentNode: TreeNode<T>;
        let i = 0;

        while(i < allTreeNodes.length) {
            currentNode = allTreeNodes[i];
            expandedNodes.push(currentNode);
            if (!currentNode.isExpanded) {
                const nextIndex = allTreeNodes.findIndex((node, index) => index > i && node.level <= currentNode.level);
                
                if(nextIndex == -1) {
                    break;
                }

                i = nextIndex;
            } else {
                i++;
            }
        }

        return expandedNodes;
    }
}