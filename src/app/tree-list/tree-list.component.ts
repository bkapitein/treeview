import { Component, ContentChild, Directive, Input, TemplateRef } from '@angular/core';
import { TreeNodeDataSource } from './tree-node-data-source';

@Directive({
  selector: '[treeNodeTemplate]'
})
export class TreeNodeDirective {}

@Component({
  selector: 'app-tree-list',
  templateUrl: './tree-list.component.html',
  styleUrls: ['./tree-list.component.scss']
})
export class TreeListComponent<T> {
  @Input()
  public dataSource!: TreeNodeDataSource<T>;

  @ContentChild(TreeNodeDirective, {read: TemplateRef})
  public treeNodeTemplate!: TemplateRef<any>;
  
  constructor() {
  }
}